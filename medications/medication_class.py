from dataclasses import dataclass, field
from typing import List

# from doctors.doctor_class import Doctor
# from persons.person_class import Person


@dataclass
class Medication():
    sort_index: float = field(init=False, default=0.0)
    name: str
    price: float
    generic: bool
    refundable: bool
    refund_percentage: float
    composition: List[str] = field(init=False, default_factory=List)

    def __post_init__(self) -> None:
        self.sort_index = self.price

    @property
    def refund_amount(self) -> float:
        if self.refundable:
            return self.price * (self.refund_percentage / 100)
        return 0

    @property
    def price_after_refund(self) -> float:
        if self.refundable:
            return self.price - self.refund_amount
        return self.price
