import json
# from medications.medication_class import Medication

def load_from_json(filename, type):
    """Loads a json file and returns its content

    Args:
        filename (File): The json file from which to fetch data
    """
    res = []
    type = type.lower()
    with open(filename, 'r') as file:
        data = json.load(file)
    for l in data:
        if type == 'customers':
            from customers.customer_class import Customer
            cust = Customer(**l)
            res.append(cust)
        elif type == 'doctors':
            from doctors.doctor_class import Doctor
            doct = Doctor(**l)
            res.append(doct)
        if type == 'medications':
            med = Medication(**l)
            res.append(med)
    return res
