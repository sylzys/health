from faker import Faker
import json
import logging
import random
from typing import Dict, List, Any

""" Will be used to generate fake doctors, customers, and medecines.
Better to use a DB in a bigger project, that's just a mock
"""

fake = Faker()
specialties = ['neuro', 'chiro', 'psycho', 'general']
import logging

# create a logger
logger = logging.getLogger('my_logger')
logger.setLevel(logging.DEBUG)

# create a stream handler to log to console
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# create a formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)

# add the handler to the logger
logger.addHandler(console_handler)
def mock_people(nb: int = 10, type: str ="customer") -> List[Any]:
    people = []
    for _ in range(nb):
        person: Dict[str, Any] = {}
        name= fake.name().split(" ")
        person['firstname'], person['lastname'], *_ = name #fake.name().split(" ")

        person['age'] = random.randint(18, 70)
        if type == 'doctor':
            person['specialty'] = random.choice(specialties)
        people.append(person)
    return people

def mock_medecines(nb: int = 10) -> List[Any]:
    meds = []
    for _ in range(nb):
        med: Dict[str, Any] = {}
        med['name'] = fake.name().split(" ")[1]
        med['price'] = random.randint(5, 50)
        med['generic'] = random.random() == 0
        med['refundable'] = random.randint(0, 1)
        med['refund_percentage'] = random.randint(10, 100) if med['refundable'] else 0
        med['composition'] = []
        meds.append(med)
    return meds

customers = mock_people()
with open('assets/customers.json', 'w') as out:
    json.dump(customers, out)

doctors = mock_people(type="doctor")
with open('assets/doctors.json', 'w') as out:
    json.dump(doctors, out)

medecines = mock_medecines()
with open('assets/medecines.json', 'w') as out:
    json.dump(medecines, out)

