from dataclasses import dataclass, field
from typing import List

from doctors.doctor_class import Doctor
from persons.person_class import Person


@dataclass
class Customer(Person):
    firstname: str
    lastname: str
    age: int
    type: str = field(init=False, default='Customer')
    doctors: List[Doctor] = field(init=False, default_factory=list)

