from dataclasses import dataclass, field
from typing import List, Optional, Any
from utils.loader import load_from_json

from persons.person_class import Person


@dataclass
class Doctor(Person):
    firstname: str
    lastname: str
    age: int
    type: str = field(init=False, default='Doctor')
    specialty: str
    patients: List[Any] = field(init=False, default_factory=list)

class DoctorList():
    __instance = None
    doctors: List[Doctor] = []
    def __new__(self) -> 'DoctorList':
        if self.__instance == None:
            from customers.customer_class import Customer
            self.__instance = super().__new__(self)
            DoctorList = load_from_json('assets/doctors.json', 'doctors')
            self.__instance.doctors = DoctorList
        return self.__instance

    def __repr__(self) -> str:
        return "\n".join([f'{x.get_fullname()} ({x.specialty.capitalize()})' for x in self.doctors])
