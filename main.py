from customers.customer_class import Customer
from doctors.doctor_class import DoctorList

# from medications import medication_class


def main() -> None:
    """
        Main function calling all other functions
    """
    c = Customer('toto', 'titi', 30)
    print(c)
    dl = DoctorList()
    print(dl)

if __name__ == "__main__":
    main()
