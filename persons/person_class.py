from abc import ABC, abstractmethod
from dataclasses import dataclass, field


@dataclass
class Person(ABC):
    firstname: str
    lastname: str
    age: int
    type: str = field(init=False)

    def get_fullname(self) -> str:
        """
            Formating first+last name, concrete method will add type of person
        """
        return f'{self.firstname} {self.lastname}'

    def __str__(self) -> str:
        return f'{self.type}: {self.firstname} {self.lastname}'
