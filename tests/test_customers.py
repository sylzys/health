import pytest

from customers.customer_class import Customer


def test_customer_creation() -> None:
    c1 = Customer('toto', 'titi', 30)
    assert c1.firstname == 'toto'
    assert c1.get_fullname() == 'toto titi'
    assert str(c1) == 'Customer: toto titi'

    c1.type = "Super Customer"
    assert str(c1) == 'Super Customer: toto titi'


